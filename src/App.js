import React, {Suspense} from 'react';
import Loading from './components/loading/Loading'
const ListView = React.lazy(() => import('./components/listView/ListView'))


function App() {
  return (
      <div>
          <Suspense fallback={<Loading />}>
              <div>
                  <ListView />
              </div>
          </Suspense>
      </div>
  );
}


export default App;
