import React from 'react'
import ListView from './ListView'
import { create } from "react-test-renderer"

it('test JSON component', () => {
	const component = create(<ListView />);
	let tree = component.toJSON();
  	expect(tree).toMatchSnapshot();
});