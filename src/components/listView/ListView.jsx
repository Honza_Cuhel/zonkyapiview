import React, { Component, Fragment } from 'react';
import Loading from '../loading/Loading'
import Modal from '../modal/Modal'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortAmountUpAlt, faSortAmountDownAlt } from '@fortawesome/free-solid-svg-icons'

const FlexBox = styled.div`
    display: flex;
    @media (max-width: 678px) {
        flex-direction: column;
      }
`

const StoryLabel = styled.label`
    font-size: 14px;
    font-weight: 300;
`

const Header = styled.h2`
    text-align: center;
    margin-top: 60px;
    font-size: 36px;
    margin-bottom: 20px;
`

const SortContainer = styled.div`
    width: 100%;
    height: 42px;
    display: flex;
    justify-content: flex-end;
    padding: 6px;
    text-align: center;
`

const SortLabel = styled.label`
  padding-left: 4px;
`

const SortLabelContainer = styled.div`
    text-align: center;
    padding: 4px 8px;
`

const ContentContainer = styled.div`
    margin-left: 20px;
    margin-top: 5px;
`

const Image = styled.img`
    max-width: 248px;
    max-height: 160px;
    border-radius: 20px;
    @media (max-width: 678px) {
        max-width: 100%;
        width: 100%;
        max-height: 303px;
        height: auto;
    }
`

const ASC_MODE = 0;
const DESC_MODE = 1;
const DURATION_KEY = 'duration'
const RATING_KEY = 'rating'
const AMOUNT_KEY = 'amount'
const DEADLINE_KEY = 'deadline'
const DAY_PUBLISHED_KEY = 'datePublished'
const NO_SORT_KEY = ''

class ListView extends Component {
  constructor(props) {
      super(props)
      this.state = {
        data: [],
        loading: true,
        selected: {},
        sortKey: NO_SORT_KEY,
        sortOrder: ASC_MODE,
      }
  }

  componentDidMount() {
    this.getData()
    setInterval(() => this.getData(), 300000)
  }

  getData = () => {
      this.setState({loading: true})
      fetch('https://api.allorigins.win/raw?url=https://api.zonky.cz/loans/marketplace/', {
          method: 'GET',
      })
      .then(response => {
          if (response.ok)
            return response.json()
      })
      .then(res => {
        let p = new Promise((resolve, reject) => {
          this.setState({data: res, loading: false})
          resolve('Success!');
        });
        p.then( value => {
          if (this.state.sortKey !== NO_SORT_KEY) {
            this.sort(this.state.sortKey, this.state.sortOrder)
          }
        },);
      })
      .catch((error) => {
        alert('Error occured while getting data.. :(')
      })
  }

  splitStartString = (s) => {
      let firstTwentyWords = s.split(' ').slice(0, 20)
      let sum = 0
       firstTwentyWords.forEach((word, i) => {
           sum += word.length
           if (sum >= 200) {
               return `${firstTwentyWords.substring(0, i).join(' ')}...`
           }
       })
       return firstTwentyWords.join(' ')
  }

  getTime = date => {
    return new Date(date.toString()).getTime()
  }

  sort = (key, mode=ASC_MODE) => {
      if (this.state.sortKEY !== NO_SORT_KEY) {
          let newData = [...this.state.data]
      
          if (key === AMOUNT_KEY) {
              newData.sort((a, b) => a[AMOUNT_KEY] - b[AMOUNT_KEY])
          } else if (key === RATING_KEY) {
              newData.sort((a, b) => a[RATING_KEY].localeCompare(b[RATING_KEY]))
          } else if (key === DURATION_KEY) {
              newData.sort((a, b) => (this.getTime(a[DEADLINE_KEY]) - this.getTime(a[DAY_PUBLISHED_KEY])) - (this.getTime(b[DEADLINE_KEY]) - this.getTime(b[DAY_PUBLISHED_KEY])))
          } else if (key === DEADLINE_KEY) {
              newData.sort((a, b) => this.getTime(a[DEADLINE_KEY]) - this.getTime(b[DEADLINE_KEY]))
          } else {
              newData.sort((a, b) => a - b)
          }
          
          if (mode === DESC_MODE)
            newData.reverse()

          this.setState({data: newData})
      }
  }

  setSortKey = Key => {
    this.setState({sortKey: Key})
    this.sort(Key, this.state.sortOrder)
  }

  setSortOrder = () => {
    const { sortOrder, sortKey } = this.state 
    let setOrder = (sortOrder+1)%2
    this.sort(sortKey, setOrder)
    this.setState({sortOrder: setOrder})
  }

  render() {
      const { data, loading, selected, sortKey, sortOrder } = this.state
      const dataLists = data.map((d, i) => {
          return (
              <li key={i} className="list-group-item list-group-item-action" data-toggle="modal" data-target="#detailModal" onClick={()=>this.setState({selected: d})}>
                <FlexBox>
                      <Image src={`https://app.zonky.cz/api${d.photos[0].url}`} alt="loan" />
                      <ContentContainer>
                          <h5>{d.name}</h5>
                          <StoryLabel>{this.splitStartString(d.story)}</StoryLabel>
                      </ContentContainer>
                  </FlexBox>
              </li>
          )
      })

      let sortBtnText = sortKey !== NO_SORT_KEY ? sortKey.toString().replace(/^\w/, c => c.toUpperCase()) : ''
    return (
        <Fragment>
            { loading ?
                <Loading />
              :
                (<div className="container">
                    <Header>Zonky API View</Header>
                    <SortContainer>
                        <SortLabelContainer>
                          <FontAwesomeIcon title="Sort order" icon={sortOrder === ASC_MODE ? faSortAmountUpAlt : faSortAmountDownAlt} style={{cursor: 'pointer'}} onClick={() => this.setSortOrder()} />
                          <SortLabel>Sort by</SortLabel>
                        </SortLabelContainer>
                        <div className="dropdown">
                          <button className="btn btn-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {sortBtnText}
                          </button>
                          <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <label className="dropdown-item" onClick={() => this.setSortKey(AMOUNT_KEY)}>Amount</label>
                            <label className="dropdown-item" onClick={() => this.setSortKey(DEADLINE_KEY)}>Deadline</label>
                            <label className="dropdown-item" onClick={() => this.setSortKey(DURATION_KEY)}>Duration</label>
                            <label className="dropdown-item" onClick={() => this.setSortKey(RATING_KEY)}>Rating</label>
                          </div>
                        </div>
                    </SortContainer>
                    <ul className="list-group list-group-flush">
                        {dataLists}
                    </ul>
                </div>)
            }
            <Modal dataJson={selected} />
        </Fragment>
    )
  }
}


export default ListView