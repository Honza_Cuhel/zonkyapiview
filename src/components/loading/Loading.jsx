import React from 'react'
import styled, {keyframes} from 'styled-components'

const keyFrameAnimation = keyframes`
  0% {
    top: 6px;
    height: 65px;
  }
  50%, 100% {
    top: 19px;
    height: 42px;
  }
`

const Container = styled.div`
    position: absolute;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: 'center';
`

const LoadingContainer = styled.div`
    display: inline-block;
    position: relative;
    margin: auto;
    width: 64px;
    height: 80px;
`

const FirstDiv = styled.div`
    display: inline-block;
    position: absolute;
    left: 6px;
    width: 8px;
    background: #000;
    animation: ${keyFrameAnimation} 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    left: 6px;
    animation-delay: -0.24s;
`

const SecondDiv = styled.div`
    display: inline-block;
    position: absolute;
    left: 6px;
    width: 8px;
    background: #000;
    animation: ${keyFrameAnimation} 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    left: 26px;
    animation-delay: -0.12s;
`
const ThirdDiv = styled.div`
    display: inline-block;
    position: absolute;
    left: 6px;
    width: 8px;
    background: #000;
    animation: ${keyFrameAnimation} 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    left: 45px;
    animation-delay: 0;
`

const Loading = () => {
    return (
        <Container>
            <LoadingContainer>
                <FirstDiv />
                <SecondDiv />
                <ThirdDiv />
            </LoadingContainer>
        </Container>
    )
}

export default Loading
