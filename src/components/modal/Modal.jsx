import React from 'react'
import ReactJson from 'react-json-view'

const Modal = (props) => {
    const { dataJson } = props
    return (
        <div className ="modal fade" id="detailModal" role="dialog" aria-labelledby="detailModal" aria-hidden="true">
            <div className ="modal-dialog" role="document">
              <div className ="modal-content">
                <div className ="modal-header">
                  <h5 className ="modal-title" id="exampleModalLabel">Detail</h5>
                  <button type="button" className ="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className ="modal-body"  >
                    <ReactJson
                        displayDataTypes={false}
                        src={dataJson} />
                </div>
                <div className ="modal-footer">
                  <button type="button" className ="btn btn-info" data-dismiss="modal" title="Cancel">Cancel</button>
                </div>
              </div>
            </div>
          </div>
    )
}

export default Modal
