import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

i18n.use(LanguageDetector).init({
	resources: {
		cs: {
			app: {

			},
		},
		en: {
			app: {
				
			}
		},
	},
	fallbackLng: 'en',
	keySeparator: false, // we use content as keys
	interpolation: {
	escapeValue: false, // not needed for react!!
	formatSeparator: ","
	},
	react: {
		wait: true
	},
});


export default i18n;
